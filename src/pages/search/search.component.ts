import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PlaystationService} from '../../app/services/playstation.Service';
import { AdMobPro } from '../../app/services/admobpro.service';
import { DetailsComponent } from '../details/details.component';

@Component({
  selector: 'selector',
  templateUrl: 'search.component.html',
})
export class SearchComponent  {
  query:string = '';
  result:any;
  products:any = [];
  loading:boolean = false;
  constructor(private _playstation: PlaystationService,
              public navCtrl:NavController,
              private _adMobPro: AdMobPro) {

              //this._adMobPro.removeAds();
              this._adMobPro.showInterstitial();
  }

  onInput(){
    this._playstation.updateUrl();
    this.loading = true;
    this.products = [];
    if( this.query.length > 0){
      this._playstation.getSearch(this.query)
          .then(data => {
              this.result = data;
              console.log(data);
              this.products = this.result.links;
              console.log(this.products);
              this.loading = false;
            });
    }else{
      this.loading = false;
    }

  }

  details( id ){
    this.navCtrl.push(DetailsComponent,{id})
  }
}
