import { Component } from '@angular/core';
import { NavController, MenuController, AlertController  } from 'ionic-angular';
import { PlaystationService } from '../../app/services/playstation.Service';
import { AdMobPro } from '../../app/services/admobpro.service'
import { ProductsComponent } from '../../pages/products/products.component';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage{
  data:any = '';
  result:any = '';
  loading:boolean = false;
  country:any = '';
  constructor(public navCtrl: NavController,
              public menuCtrl: MenuController,
              public alertCtrl: AlertController,
              private _playstation:PlaystationService,
              private _adMobPro: AdMobPro) {

      //Refresh de la url
      this._playstation.updateUrl();
      //se obtiene el pais registrado en setting
      //por defecto es Estados Unidos
      this.country = this._playstation.getSetting();
      this.loading = true;
      //Se obtiene el resultado de los descuentos
      this._playstation.getDeals().then(data => {
         this.result = data
         this.data = this.result.links;
         this.loading = false;
       },
       (error) => {
         //Captura del error
         console.log('error...',error);
         this.presentAlert();
     });


  }

  presentAlert() {
  let alert = this.alertCtrl.create({
    title: 'Error',
    subTitle: 'The promotions could not be obtained.<br> Please report the error',
    buttons: ['OK']
  });
  alert.present();
}


//Muestra la pagina de los productos de una Oferta
  listItems( id ){
     this.navCtrl.push(ProductsComponent,{id})
  }
}
