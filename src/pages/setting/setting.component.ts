import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PlaystationService } from '../../app/services/playstation.Service';
import { HomePage } from '../home/home';

@Component({
  selector: 'app-setting',
  templateUrl: 'setting.component.html',
})
export class SettingComponent implements OnInit {
  country:any = [
          { 'name': 'United states', 'key' : 'US-en'},
          { 'name': 'Colombia', 'key' : 'CO-es'},
          { 'name': 'United Kingdom', 'key' : 'GB-en'},
          { 'name': 'Argentina', 'key' : 'AR-es'},
          { 'name': 'Australia', 'key' : 'AU-en'},
          { 'name': 'België - Nederlands ', 'key' : 'BE-nl'},
          { 'name': 'Belgique - Français ', 'key' : 'BE-fr'},
          { 'name': 'Bahrain - English', 'key' : 'BH-en'},
          { 'name': 'مملكة البحرين - العربية', 'key' : 'BH-ar'},
          { 'name': 'Bolivia', 'key' : 'BO-es'},
          { 'name': 'Brazil', 'key' : 'BR-pt'},
          { 'name': 'Bulgaria', 'key' : 'BG-en'},
          { 'name': 'Canada - English', 'key' : 'CA-en'},
          { 'name': 'Canada - Français', 'key' : 'CA-fr'},
          { 'name': 'Chile', 'key' : 'CL-es'},
          { 'name': 'Costa Rica', 'key' : 'CR-es'},
          { 'name': 'Croatia', 'key' : 'HR-en'},
          { 'name': 'Cyprus', 'key' : 'CY-en'},
          { 'name': 'Czech Republic', 'key' : 'CZ-en'},
          { 'name': 'Danmark - Dansk', 'key' : 'DK-da'},
          { 'name': 'Danmark - English', 'key' : 'DK-en'},
          { 'name': 'Deutschland', 'key' : 'DE-de'},
          { 'name': 'Ecuador', 'key' : 'EC-es'},
          { 'name': 'El Salvador', 'key' : 'SV-es'},
          { 'name': 'España', 'key' : 'ES-es'},
          { 'name': 'Finland', 'key' : 'FI-en'},
          { 'name': 'France', 'key' : 'FR-fr'},
          { 'name': 'Greece', 'key' : 'GR-en'},
          { 'name': 'Guatemala', 'key' : 'GT-es'},
          { 'name': 'Honduras', 'key' : 'HN-es'},
          //{ 'name': 'Hong Kong - English', 'key' : 'HK-en'},
          { 'name': 'Hungary', 'key' : 'HU-en'},
          { 'name': 'Iceland', 'key' : 'IS-en'},
          { 'name': 'India', 'key' : 'In-en'},
          //{ 'name': 'Indonesia', 'key' : 'ID-en'},
          { 'name': 'Ireland', 'key' : 'IE-en'},
          { 'name': 'Israel', 'key' : 'IL-en'},
          { 'name': 'Italia', 'key' : 'IT-it'},
          { 'name': 'Mexico', 'key' : 'MX-es'},
          { 'name': 'Panama', 'key' : 'PA-es'},
          { 'name': 'Paraguay', 'key' : 'PY-es'},
          { 'name': 'Peru', 'key' : 'PE-es'},
          { 'name': 'Uruguay', 'key' : 'UY-es'},
        ];
  check:any = 'US-en';
  constructor(private _playstationService: PlaystationService,
              public navCtrl: NavController) {
                //this._adMobPro.removeAds();

  }

  ngOnInit(){
    if(!localStorage.getItem('store')){
      this.check = 'US-en';
    }else{
      this.check = localStorage.getItem('store')
    }
  }

  save( value ){
    this._playstationService.setSetting(value);
    this.navCtrl.setRoot(HomePage);
  }
}
