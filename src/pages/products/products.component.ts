import { Component, ViewChild, OnInit } from '@angular/core';
import { Content, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { PlaystationService } from '../../app/services/playstation.Service';
import { DetailsComponent } from '../details/details.component';

import { trigger, state, style, transition, animate } from '@angular/animations'

@Component({
  selector: 'app-products',
  templateUrl: 'products.component.html',
  animations: [
    trigger('flyInOut', [
      state('in', style({transform: 'translateX(0)'})),
      transition('void => *', [
        style({transform: 'translateX(-100%)'}),
        animate(1000)
      ]),
      transition('* => void', [
        animate(100, style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class ProductsComponent implements OnInit {
  @ViewChild(Content) content: Content;
  products:any = [];
  title:string = '';
  result:any = '';
  total:number;
  start:number = 0;
  id:string;
  sort:string = "null";
  sort1:string = 'price,desc';
  direction:string = null;
  facets:any = [];
  game_type_result:string;
  game_type_input:string;
  platform_input:string;
  platform_result:string;
  query:string= '';
  loading:boolean = false;
  constructor(public navCtrl: NavController,
               public navParams:NavParams,
               public menuCtrl: MenuController,
               public alertCtrl: AlertController,
               public _playstation:PlaystationService) {

           this.id = this.navParams.get("id");
           this.loading = true;
  }

  toggleRightMenu() {
      this.menuCtrl.toggle('right');
  }

  ngOnInit() {

    this._playstation.getProducts(this.id, this.start)
              .then(data => {
                  this.result = data;
                  this.products = this.result.links;
                  this.title = this.result.name;
                  console.log(this.result );
                  this.facets = this.result.attributes.facets;
                  this.game_type_result = this.facets.game_content_type;
                  this.platform_result = this.facets.platform;
                  this.total = this.result.total_results;
                    this.loading = false;
                });

  }

  doInfinite(infiniteScroll) {
    //console.log('Begin async operation');
    let result;

      this.start += 30;
      if(this.start < this.total){
        this._playstation.getProducts(this.id, this.start, this.sort, this.direction, this.query).then(data => {
            result = data
            for (let item of result.links) {
                  this.products.push(item);
            }
            infiniteScroll.complete();
            //console.log('Async operation has ended');
        });
      }else{
        infiniteScroll.complete();
      }

  }

  sortBy(){
    this.products = [];
    this.loading = true;
    let arrayOfStrings = this.sort1.split(',');
    this.sort = arrayOfStrings[0];
    this.direction = arrayOfStrings[1];
    this.start = 0;

    this._playstation.getProducts(this.id, this.start, this.sort, this.direction, this.query)
              .then(data => {
                  this.result = data;
                this.loading = false;
                  this.products = this.result.links;
                  this.total = this.result.total_results;
                });
    this.content.scrollToTop();
    this.toggleRightMenu();
  }


/*
  click(){
    this.start = 0;
    console.log(this.game_type_input.toString());
    this.query += '&game_content_type='+this.game_type_input.toString();
    this._playstation.getProducts(this.id, this.start, this.sort, this.direction, this.query)
              .then(data => {
                  this.result = data;
                  console.log("click");
                  console.log(this.result);
                  this.products = this.result.links;
                  this.total = this.result.total_results;
                });

  }
  */

  sortByQuery(){
    this.products = [];
    this.loading = true;
    this.query = '';
    if(this.game_type_input != undefined ){
      this.query = '&game_content_type='+this.game_type_input.toString();
    }

    if(this.platform_input != undefined ){
      this.query += '&platform='+this.platform_input.toString();
    }

    this.start = 0;
    this._playstation.getProducts(this.id, this.start, this.sort, this.direction, this.query)
              .then(data => {
                  this.result = data;
                  this.loading = false;
                  this.products = this.result.links;
                  this.total = this.result.total_results;
                });
    this.content.scrollToTop();
    this.toggleRightMenu();
  }

  details( id ){
    this.navCtrl.push(DetailsComponent,{id})
  }
}
