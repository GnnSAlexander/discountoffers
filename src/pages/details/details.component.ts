import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { PlaystationService } from '../../app/services/playstation.Service';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  templateUrl: 'details.component.html',
})
export class DetailsComponent{
  id:string;
  loading:boolean = false;
  data:any;
  stars:any = [];
  constructor(public navCtrl: NavController,
               public navParams:NavParams,
               public alertCtrl: AlertController,
               public _playstation:PlaystationService,
               private iab: InAppBrowser) {

                 this.id = this.navParams.get("id");
                 this.loading = true;
                 this._playstation.getProduct(this.id)
                           .then(data => {
                               this.data = data;
                               console.log(this.data);
                               this.loading = false;
                             });

  }
/*
  ngOnInit(){
    this._playstation.getProduct(this.id)
              .then(data => {
                  this.data = data;
                  console.log(this.data);
                  this.loading = false;
                });
  }
*/

 openBrowser(url){

    this.iab.create(`https://store.playstation.com/#!/cid=${url}`,'_system');
 }


}
