import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class PlaystationService {

//STORE-MSF75508-GAMESPECIALOFF for EU
  URLBASE:string = 'https://store.playstation.com/chihiro-api/viewfinder/';
  URLSEARCH:string = 'https://store.playstation.com/chihiro-api/search/'
  VERSION:string = '999';
  url:string = '';
  urlBase:string = '';
  urlSearch:string = '';
  idUs:string = 'STORE-MSF77008-WEEKLYDEALS';
  idEu:string = 'STORE-MSF75508-GAMESPECIALOFF';
  result:any = '';
  us:any = ['CO','US','AR','BO','BR','CA','CL','CR','EC','SV','GT','HN','MX','PA','PY','PE','UY'];
  eu:any = ['ES', 'GB','AU','BE','BH','BG','HR','CY','CZ','DK', 'DE','FI','FR','GR','HK','HU','IS',
            'IN','ID','IE','IL','IT'];
  constructor(private http: Http) {

  }

  updateUrl(){
    //Se Inicializa las variables en blanco
    this.url = '';
    this.urlBase = '';
    this.urlSearch = '';
    //Obteniendo la configuracion del pais
    var conf = this.getSetting();
    var country = `${conf[0]}/${conf[1]}`;
    this.url +=`${this.URLBASE}${country}/${this.VERSION}`;
    this.urlBase += `${this.URLBASE}${country}/${this.VERSION}/`;
    this.urlSearch += `${this.URLSEARCH}${country}/${this.VERSION}`;

    //Buscando ID del pais para generar la busqueda por region (America y Europa)
    if(this.us.some(x => x == (conf[0]))){
      this.url += `/${this.idUs}`;
    }else{
      this.url += `/${this.idEu}`;
    }
  }


  getSetting(){
    if( !localStorage.getItem('store') ){
     return ['US','en'];
    //return ['ES','es'];
    }else{
      let store = localStorage.getItem('store').split("-");
      return store;
    }
  }

  setSetting( store:string ){
    localStorage.setItem('store',store);
  }

  getDeals(){
    return new Promise((resolve,reject) => {
        this.http.get(this.url).subscribe(data => {
          resolve(data.json());
        },
        (err) => { reject(err);});
      });
  }

  getProduct( id ){
    let url = this.urlBase+id;
    return new Promise((resolve, reject) => {
        this.http.get(url).subscribe(data => {
          resolve(data.json());
        },
        (err) => { reject(err);});
      });
  }

  getProducts( id, start, sort = null, direction = null, query = null){
    let url = this.urlBase+id+'?size=30&gkb=1&geoCountry=CO&start='+start
    if(sort != null && direction != null){
        url += '&sort='+sort+'&direction='+direction;
    }

    if(query != null){
      url += query;
    }
    return new Promise((resolve, reject) => {
        this.http.get(url).subscribe(data => {
          resolve(data.json());
        },
        (err) => { reject(err); });
      });
  }

  getSearch( q ){
    let url = `${this.urlSearch}/${q}?bucket=games&size=30`;
    //console.log(url);
    return new Promise((resolve, reject) => {
        this.http.get(url).subscribe(data => {
          resolve(data.json());
        },
        (err) => { reject(err); });
      });
  }

}
