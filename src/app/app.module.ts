import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ValuesPipe } from './pipes/values.pipes';
import { StarRatingPipe } from './pipes/star_rating.pipes';
import { SinfotoPipe } from './pipes/sinfoto.pipe';

import { PlaystationService} from '../app/services/playstation.Service';
import { AdMobPro } from '../app/services/admobpro.service';

import { ProductsComponent } from '../pages/products/products.component';
import { HomePage } from '../pages/home/home';
import { SearchComponent } from '../pages/search/search.component';
import { DetailsComponent } from '../pages/details/details.component';
import { SettingComponent } from '../pages/setting/setting.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { OneSignal } from '@ionic-native/onesignal';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRate } from '@ionic-native/app-rate';

/*import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

export const firebaseConfig = {
  apiKey: "AIzaSyD2oGX-O9TQW7mtQ0hjFoOu1yrKd1Q-DDc",
  authDomain: "discount-offers-c4adc.firebaseapp.com",
  databaseURL: "https://discount-offers-c4adc.firebaseio.com",
  projectId: "discount-offers-c4adc",
  storageBucket: "discount-offers-c4adc.appspot.com",
  messagingSenderId: "492846654432"
};*/

@NgModule({
  declarations: [
    MyApp,
    ValuesPipe,
    StarRatingPipe,
    SinfotoPipe,
    ProductsComponent,
    HomePage,
    SearchComponent,
    DetailsComponent,
    SettingComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp)
    //AngularFireModule.initializeApp(firebaseConfig),
    //AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProductsComponent,
    HomePage,
    SearchComponent,
    DetailsComponent,
    SettingComponent
  ],
  providers: [
    PlaystationService,
    AdMobPro,
    AppRate,
    StatusBar,
    InAppBrowser,
    OneSignal,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
