import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sinfoto'
})
export class SinfotoPipe implements PipeTransform {

  transform(value: any[], details: boolean): string {
    let noimage = 'assets/img/default.png';
    if(!value)
        return noimage;

    if(details){
      return (value.length > 2) ? value[3].url : noimage;
    }else{
      return (value.length > 2) ? value[2].url : noimage;
    }



  }

}
