import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'starRating'
})
export class StarRatingPipe implements PipeTransform {
  transform(value): any {
    value = parseFloat(value);
    let halfStar = '';
    console.log(value);
      let fullStar =  new Array(Math.floor(Math.round(value) + 1)).join('<i class="fa fa-star fa-2x"></i>');
      let total = Math.round(value);
      if(total != 5){
        total += 1;
         halfStar = ((value%1) === 0) ? '<i class="fa fa-star-half-empty fa-2x"></i>': '<i class="fa fa-star-o fa-2x"></i>';
      }
      var noStar =  (total !=5 )?  new Array(Math.floor(5+ 1 - value)).join('<i class="fa fa-star-o fa-2x"></i>'):'';
    return fullStar+halfStar+noStar;
  }
}
