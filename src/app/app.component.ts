import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppRate } from '@ionic-native/app-rate';
import { OneSignal } from '@ionic-native/onesignal';

import { HomePage } from '../pages/home/home';
import { SearchComponent } from '../pages/search/search.component';
import { SettingComponent } from '../pages/setting/setting.component';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, icon:string, component: any}>;

  constructor(public platform: Platform, public appRate: AppRate,
              public statusBar: StatusBar, public splashScreen: SplashScreen,
              public alertCtrl: AlertController, private oneSignal: OneSignal) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home',icon: 'logo-playstation', component: HomePage },
      { title: 'Search',icon: 'search', component: SearchComponent },
      //{ title: 'Wishlist',icon: 'heart', component: SearchComponent },
      { title: 'Setting',icon: 'cog', component: SettingComponent }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.backgroundColorByHexString('#a21919');
      this.splashScreen.hide();

      //push notification
      //this.handlerNotification();

      //app rate
      this.configAppRate();

    });
  }

  private configAppRate(){
      this.appRate.preferences = {
       openStoreInApp: false,
       displayAppName: 'Ps Deals + Discount offers',
       usesUntilPrompt: 3,
       promptAgainForEachNewVersion: false,
       storeAppURL: {
         ios: '',
         android: 'market://details?id=com.gnnslxndr.gnnslxndr'
       },
       customLocale: {
         title: 'Do you enjoy %@?',
         message: 'It won’t take more than a minute. Thanks for your support!',
         cancelButtonLabel: 'No, Thanks',
         laterButtonLabel: 'Remind Me Later',
         rateButtonLabel: 'Rate It Now'
       },
       callbacks: {
         onRateDialogShow: function(callback){
           console.log('rate dialog shown!');
         },
         onButtonClicked: function(buttonIndex){
           console.log('Selected index: -> ' + buttonIndex);
         }
       }
     };

     // Opens the rating immediately no matter what preferences you set
     this.appRate.promptForRating(false);
  }

  private handlerNotification(){
    //Inicializando plugin
    this.oneSignal.startInit('8c309cea-5999-4103-81e5-b73b6dbabb31', '492846654432');
    this.oneSignal.setSubscription(true);
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

    this.oneSignal.handleNotificationReceived()
    .subscribe( jsonData => {
      let alert = this.alertCtrl.create({
        title: jsonData.payload.title,
        subTitle: jsonData.payload.body,
        buttons: ['OK']
      });
      alert.present();
      console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    });

    this.oneSignal.handleNotificationOpened().subscribe(() => {
      // do something when a notification is opened
    });

    this.oneSignal.endInit();
  }

  rate(){
    this.appRate.navigateToAppStore();
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
